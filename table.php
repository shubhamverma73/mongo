<?php
	require 'vendor/autoload.php';
	// connect to mongodb
	$m = new MongoDB\Client("mongodb://127.0.0.1:27017") or die ('connection to db failed');
	// select a database
	$db = $m->mymongo;


	/* =================== Create Collections ===================== */
   	function createCollection($db) {
		$db->createCollection('contacts', [
		    'collation' => ['locale' => 'en_US'],
		]);
	}
	//createCollection($db);


	function deleteCollection($db) {
		$drop = $db->dropCollection('test');
		var_dump($drop);

		foreach ($db->listCollections() as $data) {
			echo '<pre>';
			print_r($data);
			echo '</pre>';
		}
	}
	//deleteCollection($db);



	function createIndex($db) {
		$collection = $db->contacts;

		$collection->createIndex(
		    ['first_name' => 1],
		    [
		        'collation' => ['locale' => 'en_US'],
		        'unique' => true,
		    ]
		);
	}
	//createIndex($db);


	function find($db) {
		$collection = $db->contacts;

		$document = $collection->find(['first_name' => 'Adam']);
		echo '<pre>';
		print_r($document);
	}
	//find($db);


	function createCollectionAdvanced($db) {
		$db->createCollection('test', [
		    'validator' =>
			['$jsonSchema' => 
				[
				'bsonType' => "object",
				'required' => [ "phone" ],
				'properties' => 
					[
						'phone' => 
						[
							'bsonType' =>"string",
							'description' => "must be a string and is required"
						],
							'email' =>
						[
							'bsonType' => "string",
				            'pattern' => "@mongodb\.com$",
				            'description' => "must be a string and match the regular expression pattern"
						]
					]
				]
			],
		]);
	}
	//createCollectionAdvanced($db);


?>