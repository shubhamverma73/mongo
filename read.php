<?php
require('connection.php');

$collection = 'user';
$filter = ['status' => true];
$options = ['limit' => 2];

$query = new MongoDB\Driver\Query($filter, $options);
$data = $manager->executeQuery("$dbname.$collection", $query);

foreach ($data as $user) {
	echo nl2br('<b>Name: </b>'.$user->name. ', <b> Email: </b>'.$user->email. ', <b>Address: </b>'.$user->address. "\n". "\n");
}