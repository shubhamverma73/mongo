<?php
	require 'vendor/autoload.php';
	// connect to mongodb
	$m = new MongoDB\Client("mongodb://127.0.0.1:27017") or die ('connection to db failed');
	// select a database
	$db = $m->mymongo;


   	function createOne($db) {
		$collection = $db->product;

		$document = array( 
		  "name" => "Gavava", 
		  "type" => "fruits", 
		  "status" => true
		);

		$insertOneResult = $collection->insertOne($document);
		echo "Document inserted successfully";
		printf($insertOneResult->getInsertedCount());
		print_r($insertOneResult->getInsertedId());
	}
	//create($db);


	function createMany($db) {
		$collection = $db->product;

		$document = array([
		  "name" => "Strawbery", 
		  "type" => "fruits", 
		  "status" => true
		],[
		  "name" => "Banana", 
		  "type" => "fruits", 
		  "status" => true
		]);

		$insertManyResult = $collection->insertMany($document);
		echo "Document inserted successfully";
		echo '<pre>';
		printf($insertManyResult->getInsertedCount());
		print_r($insertManyResult->getInsertedIds());
	}
	//createMany($db);


	function findOne($db) {
		$collection = $db->product;

		$document = $collection->findOne(['status' => true]);
		echo '<pre>';
		print_r($document);
	}
	//findOne($db);


	function find($db) { //For multiple
		$collection = $db->product;

		$documents = $collection->find(['status' => true], ['limit' => 3, 'sort' => ['name' => 1]]); //-1 are decending and 1 are asc
		foreach ($documents as $document) {
		    echo $document['name'].'<br>';
		}
	}
	//find($db);


	function updateOne($db) {
		$collection = $db->product;

		$updateResult = $collection->updateOne(
			['name' => 'Cherries'], //1st parameter is where
			['$set' => ['category' => ['category_name' => 'fruits', 'category_id' => 1]]] // and 2nd is value for update
		);

		printf("Matched %d document(s)\n", $updateResult->getMatchedCount());
		printf("Modified %d document(s)\n", $updateResult->getModifiedCount());
	}
	/* ========================= This function update only 1 row even more than 1 rows matches in filter =================== */
	//updateOne($db);


	function updateMany($db) {
		$collection = $db->product;

		$updateResult = $collection->updateMany(
			['status' => true],
			['$set' => ['category' => ['category_name' => 'fruits', 'category_id' => 1]]]
		);
		printf("Matched %d document(s)\n", $updateResult->getMatchedCount());
		printf("Modified %d document(s)\n", $updateResult->getModifiedCount());
	}
	/* ========================= This function update all rows matches in filter =================== */
	//updateMany($db);


	function replaceOne($db) {
		/* Replacement operations are similar to update operations, but instead of updating a document to include new fields or new field values, a replacement operation replaces the entire document with a new document, but retains the original document’s _id value. Means you can ADD or REMOVE fields*/
		$collection = $db->product;

		$replaceResult = $collection->replaceOne(
			['name' => 'Blueberries'],
			['name' => 'Blueberries', 'status' => true, 'date' => date('Y-m-d')]
		);
		printf("Matched %d document(s)\n", $replaceResult->getMatchedCount());
		printf("Modified %d document(s)\n", $replaceResult->getModifiedCount());
	}
	//replaceOne($db);


	function upsert($db) {
		/* Update and replace operations support an upsert option. When upsert is true and no documents match the specified filter, the operation creates a new document and inserts it. If there are matching documents, then the operation modifies or replaces the matching document or documents. */
		$collection = $db->product;

		$updateResult = $collection->updateOne(
		    ['name' => 'Blueberries'],
		    ['$set' => ['name' => 'Blueberries', 'status' => true, 'date' => date('Y-m-d')]],
		    ['upsert' => true]
		);

		printf("Matched %d document(s)\n", $updateResult->getMatchedCount());
		printf("Modified %d document(s)\n", $updateResult->getModifiedCount());
		printf("Upserted %d document(s)\n", $updateResult->getUpsertedCount());
	}
	//upsert($db);

	function deleteOne($db) {
		$collection = $db->category;

		$deleteResult = $collection->deleteOne(
			['name' => 'Gadgets']
		);
		printf("Deleted %d document(s)\n", $deleteResult->getDeletedCount());
		/* ========================= This function delete only 1 row even more than 1 rows matches in filter =================== */
	}
	//deleteOne($db);


	function deleteMany($db) {
		$collection = $db->category;

		$deleteResult = $collection->deleteMany(
			['name' => 'Gadgets']
		);
		printf("Deleted %d document(s)\n", $deleteResult->getDeletedCount());
		/* ========================= This function delete all rows matches in filter =================== */
	}
	//deleteMany($db);

	function dropDb($db) {
		$collection = $db->delete;
		$result = $collection->drop();
		var_dump($result);
	}
	//dropDb($db);
?>