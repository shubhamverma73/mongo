<?php
require('connection.php');

$collection = 'product';

$bulkWrite = new MongoDB\Driver\BulkWrite;
$filter = ['name' => 'Strawberry'];
$update = ['$set' => ['name' => 'Strawberries']];
$options = ['multi' => false, 'upsert' => false];
$bulkWrite->update($filter, $update, $options);
$update_data = $manager->executeBulkWrite("$dbname.$collection", $bulkWrite);
echo '<pre>';
print_r($update_data);